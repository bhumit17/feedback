//
//  OneFlowConfiguration.swift
//  Feedback
//
//  Created by Bhumit Mehta on 13/12/21.
//

import Foundation
import UIKit

public final class OneFlowThemeConfiguration: NSObject {
    
    var primaryTitleFont : UIFont = UIFont.systemFont(ofSize: 18)
    var descriptionFont : UIFont = UIFont.systemFont(ofSize: 14)
    var optionLableFont : UIFont = UIFont.systemFont(ofSize: 14)
    
    public init(primaryFont: UIFont?, secondaryFont: UIFont?, optionFont: UIFont?) {
        
        if let primaryFontObj : UIFont = primaryFont {
            primaryTitleFont = primaryFontObj
        }
       
        if let secondaryFontObj : UIFont = secondaryFont {
            descriptionFont = secondaryFontObj
        }
      
        if let optionFontObj : UIFont = optionFont {
            optionLableFont = optionFontObj
        }
    }
    
    public convenience override init() {
        self.init(primaryFont: nil, secondaryFont: nil, optionFont: nil)
    }

}
